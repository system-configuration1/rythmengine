# RythmEngine


## Description
RythmEngine is designed to be a fully open-source level editor that allows you to make levels in the spirit of geometry dash. This is mainly just a project for me to mess around with on my free time. It4 is not designed to be a 1 to 1 recreation of geometry dash like something such as OpenGD. It is designed to have a more open and modular approach to level creation. Game will also be 2.5D cause why not?

## Installation
For now install Godot 4 and run the project through that.


## Support
Create an issue on gitlab.

## Roadmap
- Create a level loader which takes in the level as a json file and plays it
- Create basic objects and start trigger
- Create a basic player

## Contributing
The project is written in GDScript for Godot 4. Feel free to open pull requests and I'll take a look at them when I have time.

## License
GPL

## Project status
Whenever it fits into my schedule. I have a lot of schoolwork and stuff going on so this isn't a huge priority right now. 
