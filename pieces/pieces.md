## Pieces

This folder contains all data for pieces. The components folder will contain different attributes and behaviors that the pieces might have. Those components can then be attached to the pieces themselves. There are several main types of pieces...


### Blocks


### Ramps


### Decoration
Non-collidable (mainly because of complex hitboxes)


### Triggers 
Invisible in game (probably through a component) and project a line in the editor to show duration (includes speed change and mode change). Maybe give these these triggers handles that they can drag to create an area for on contact effect?
